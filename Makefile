CFLAGS=-Wall -Wextra -std=c99 -Werror -ggdb -I$(shell pwd)
VALGRIND=valgrind --leak-check=full

all: queueADT.o tests

tests: test1 test2 test3 test4

tests/1/test: queueADT.o tests/1/test.c
	$(CC) $(CFLAGS) -o $@ $^

tests/2/test: queueADT.o tests/2/test.c
	$(CC) $(CFLAGS) -o $@ $^

tests/3/test: queueADT.o tests/3/test.c
	$(CC) $(CFLAGS) -o $@ $^

tests/4/test: queueADT.o tests/4/test.c
	$(CC) $(CFLAGS) -o $@ $^

test1: tests/1/test
	$(VALGRIND) tests/1/test > tests/1/stdout.actual
	diff tests/1/stdout tests/1/stdout.actual

test2: tests/2/test
	$(VALGRIND) tests/2/test > tests/2/stdout.actual
	diff tests/2/stdout tests/2/stdout.actual

test3: tests/3/test
	$(VALGRIND) tests/3/test > tests/3/stdout.actual
	diff tests/3/stdout tests/3/stdout.actual

test4: tests/4/test
	$(VALGRIND) tests/4/test > tests/4/stdout.actual
	diff tests/4/stdout tests/4/stdout.actual

fix-formatting:
	clang-format -i *.c *.h

clean:
	rm *.o

.PHONY: clean fix-formatting test1 test2 test3 test4
