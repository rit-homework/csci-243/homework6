#define _QUEUE_IMPL_
typedef struct {
    /// A comparator function which defines what larger and smaller mean. This
    /// is null when a comparator isn't provided.
    int (*comparator)(const void *a, const void *b);

    /// The elements stored in the queue.
    void **elements;

    /// The maximum number of elements which can be placed into the elements
    /// area without another allocation.
    int cap;

    /// The number of meaningful elements in the queue.
    int length;
} Queue;

typedef struct {
    /// A reference to the implementation of the queue.
    Queue *queue;
} QueueADT;

typedef int (*Comparator)(const void *a, const void *b);

#include "queueADT.h"
#include <assert.h>
#include <stdlib.h>

#define PREFIX(x) que_##x

/// A helper function to get the index of the left child given the parent index.
static int left_child(int parent_idx) { return 2 * parent_idx + 1; }

/// A helper function to get the index of the right child given the parent
/// index.
static int right_child(int parent_idx) { return left_child(parent_idx) + 1; }

/// A helper function to get the index of the parent given the index of one of
/// the children.
static int parent(int child_idx) { return (child_idx - 1) / 2; }

/// Returns true when the rank of the child element at the index is larger or
/// equal to the rank of the parent element.
static bool is_valid(Queue *queue, int idx) {
    int parent_idx = parent(idx);

    void *child_element = queue->elements[idx];
    void *parent_element = queue->elements[parent_idx];

    int diff = queue->comparator(child_element, parent_element);

    // Valid when child_element is larger than or equal to the parent element.
    return diff >= 0;
}

/// A helper to swap elements at the specified indices.
static void swap(void **items, int a_idx, int b_idx) {
    void *tmp = items[a_idx];
    items[a_idx] = items[b_idx];
    items[b_idx] = tmp;
}

/// Allocates and initializes a new queue. Returns null if an allocation fails.
Queue *new (Comparator cmp) {
    Queue *queue = malloc(sizeof(Queue));
    if (queue == NULL) {
        return NULL;
    }

    queue->comparator = cmp;
    queue->length = 0;

    // Allocate Space
    queue->cap = 1;
    queue->elements = malloc(sizeof(void *) * queue->cap);
    if (queue->elements == NULL) {
        free(queue);
        return NULL;
    }

    return queue;
}

/// Frees a queue and all it's underlying memory.
void free_queue(Queue *queue) {
    // Free Elements
    free(queue->elements);

    // Free Self
    free(queue);
}

/// Doubles the size of the underlying array.
bool resize(Queue *queue) {
    int new_cap = queue->cap * 2;

    void **new_elems = realloc(queue->elements, new_cap * sizeof(void *));
    if (new_elems == NULL) {
        return false;
    }

    queue->cap = new_cap;
    queue->elements = new_elems;

    return true;
}

QueueADT PREFIX(create)(Comparator cmp) {
    // Create Queue
    Queue *impl = new (cmp);

    QueueADT queue = {.queue = impl};

    return queue;
}

void PREFIX(destroy)(QueueADT queue) { free_queue(queue.queue); }

void PREFIX(clear)(QueueADT queue) { queue.queue->length = 0; }

bool PREFIX(empty)(QueueADT queue) { return queue.queue->length == 0; }

/// Sifts the newly added value upwards until the heap invariant is met again.
static void sift_up(Queue *queue, int idx) {
    while (!is_valid(queue, idx)) {
        // Swap With Parent
        int parent_idx = parent(idx);
        swap(queue->elements, idx, parent_idx);

        // Check Whether Parent Maintains Invariant
        idx = parent_idx;
    }
}

void PREFIX(insert)(QueueADT queue_container, void *data) {
    Queue *queue = queue_container.queue;

    if (queue->cap < queue->length + 1) {
        // Grow Queue
        resize(queue);
    }

    // Add Element
    int new_item_idx = queue->length;
    queue->elements[new_item_idx] = data;
    queue->length += 1;

    if (queue->comparator != NULL) {
        // Maintain Heap Invariant
        sift_up(queue, new_item_idx);
    }
}

/// Sifts a newly inserted min value downwards until the heap invariant is met
/// again.
static void sift_down(Queue *queue, int idx) {
    int left_idx = left_child(idx);
    int right_idx = right_child(idx);

    // The smallest of the values at the left_idx, right_idx and idx. We start
    // by assuming that idx is the smallest, changing if needed.
    int smallest_idx = idx;

    if (left_idx < queue->length &&
        queue->comparator(queue->elements[left_idx],
                          queue->elements[smallest_idx]) < 0) {
        // The left index is larger than the parent.
        smallest_idx = left_idx;
    }

    if (right_idx < queue->length &&
        queue->comparator(queue->elements[right_idx],
                          queue->elements[smallest_idx]) < 0) {
        // the right index is larger than the parent and left index.
        smallest_idx = right_idx;
    }

    if (smallest_idx != idx) {
        // The smallest value is not the parent, swap.
        swap(queue->elements, smallest_idx, idx);

        // Ensure Invariant
        sift_down(queue, smallest_idx);
    }
}

void *PREFIX(remove)(QueueADT queue_container) {
    Queue *queue = queue_container.queue;

    if (queue->comparator == NULL) {
        // Remove First Item
        int removing_idx = 0;
        void *removing_value = queue->elements[removing_idx];

        // Move All Elements Backwards One Index
        for (int i = 1; i < queue->length; i++) {
            swap(queue->elements, i, i - 1);
        }

        queue->length -= 1;

        return removing_value;
    }

    assert(queue->length > 0);

    // Get Value
    int removing_idx = 0;
    void *removing_value = queue->elements[removing_idx];

    // Swap With Last
    int last_idx = queue->length - 1;
    swap(queue->elements, removing_idx, last_idx);

    // Remove Last Element
    queue->length -= 1;

    sift_down(queue, removing_idx);

    return removing_value;
}
